Summary: Dynamic Kernel Module Support Framework
Name: dkms
Version: 3.1.5
Release: 1
License: GPL-2.0-or-later
BuildArch: noarch
URL: https://github.com/dell/dkms
Source0: https://github.com/dell/dkms/archive/refs/tags/v%{version}.tar.gz
# because Mandriva calls this package dkms-minimal
Provides: dkms-minimal = %{version}
Requires: coreutils
Requires: cpio
Requires: findutils
Requires: gawk
Requires: gcc
Requires: clang llvm lld
Requires: grep
Requires: gzip
Requires: kernel-devel
Requires: sed
Requires: tar 
Requires: which
Requires: bash > 1.99
Requires:       kmod
BuildRequires:          systemd
%{?systemd_requires}
Requires: kernel-devel
Recommends:     openssl

%description
This package contains the framework for the Dynamic Kernel Module Support (DKMS)
method for installing module RPMS as originally developed by Dell.

%prep
%autosetup -p1

%install
make install-redhat DESTDIR=%{buildroot}
sed -i -e 's/# modprobe_on_install="true"/modprobe_on_install="true"/g' %{buildroot}%{_sysconfdir}/%{name}/framework.conf

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun %{name}.service

%files
%license COPYING
%doc README.md images
%{_prefix}/lib/kernel/install.d/40-%{name}.install
%dir %{_prefix}/lib/dkms
%{_prefix}/lib/dkms/common.postinst
%{_mandir}/man8/dkms.8*
%{_sbindir}/%{name}
%{_sharedstatedir}/%{name}
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/framework.conf
%dir %{_sysconfdir}/%{name}/framework.conf.d
%{_datadir}/bash-completion/completions/%{name}
%{_datadir}/zsh/site-functions/_%{name}
%{_unitdir}/%{name}.service

%changelog
* Thu Jan 30 2025 Funda Wang <fundawang@yeah.net> - 3.1.5-1
- update to 3.1.5

* Fri Oct 20 2023 chenyaqiang <chengyaqiang@huawei.com> - 3.0.12-1
- update to 3.0.12

* Tue Sep 19 2023 liyunfei <liyunfei@huawei.com> - 2.6.1-8
- Add clang compile support

* Mon Mar 27 2023 wangkai <wangkai385@h-partners.com> - 2.6.1-7
- Change deprecated egrep for grep -E

* Mon Sep 7 2020 Ge Wang <wangge20@huawei.com> - 2.6.1-6
- Modify the Source0 Url

* Fri Feb 14 2020 Tianfei <tianfei16@huawei.com> - 2.6.1-5
- Package init
